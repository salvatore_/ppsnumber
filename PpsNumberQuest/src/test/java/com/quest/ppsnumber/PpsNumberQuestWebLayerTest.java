package com.quest.ppsnumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
//import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
//import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quest.ppsnumber.controller.PpsNumberController;
import com.quest.ppsnumber.model.PpsNumberModel;
import com.quest.ppsnumber.utils.UtilsDate;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PpsNumberQuestWebLayerTest {
	 
	@Autowired private ObjectMapper objectMapper; 
	@Autowired
    private PpsNumberController ppsNumberController;
	
	@Autowired
    private MockMvc mockMvc;
	@Test
    public void contextLoads() throws Exception {
		assertThat(ppsNumberController).isNotNull();
    }
	@Test
    public void testPageindex() throws Exception {
		String result="<!DOCTYPE html>\n" + 
				"<html>\n" + 
				"<head>\n" + 
				"<meta charset=\"UTF-8\">\n" + 
				"<title>Index</title>\n" + 
				"</head>\n" + 
				"<body>\n" + 
				"<h1>Select your operation </h1>\n" + 
				"\n" + 
				"<a href=\"/loadPageInsertPpsNumber\">Add new record</a>\n" + 
				"\n" + 
				"<a href=\"/view_records\">View records</a>\n" + 
				"</body>\n" + 
				"</html>";
        this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString(result)));
    }
	 
	@Test
	@Sql(scripts = "/insert-ppsnumber.sql")
	@DirtiesContext
    public void testViewAllData() throws Exception {	
        
        mockMvc.perform(get("/view_records"))
        .andExpect(status().isOk())
        .andExpect(view().name("/view_records"))
        .andExpect(model().attribute("ppsNumberModelList",hasSize(1)));
    }
	
	@Test
	@DirtiesContext
    public void testCreatePpsNumber() throws Exception {
		PpsNumberModel ppsNumberModel = new PpsNumberModel();
		ppsNumberModel.setName("Antonio");
		ppsNumberModel.setMobilePhoneNumber("081234567893");
		ppsNumberModel.setPpsNumber("1234567G");
		ppsNumberModel.setDateOfBirth(UtilsDate.getDataFromString("10/10/1980"));
		   mockMvc.perform(post("/api/addnewrecord")
		        .contentType("application/json")
		        .content(objectMapper.writeValueAsString(ppsNumberModel)))
		        .andExpect(status().isOk());
    }
}
