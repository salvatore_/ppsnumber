package com.quest.ppsnumber;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.TransactionSystemException;

import com.quest.ppsnumber.model.MessageModel;
import com.quest.ppsnumber.model.PpsNumberModel;
import com.quest.ppsnumber.service.PpsNumberService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PpsNumberQuestApplicationTests {

	@Autowired
	private PpsNumberService ppsNumberService;

	@Test
	public void contextLoads() {

	}

	@Test
	@DirtiesContext
	public void addNewPpsnumberCaseOk() {
		PpsNumberModel ppsNumberModel = new PpsNumberModel();
		ppsNumberModel.setName("Salvo");
		ppsNumberModel.setMobilePhoneNumber("081234567890");
		ppsNumberModel.setPpsNumber("1234567A");
		ppsNumberModel.setDateOfBirth(new Date());
		MessageModel messageModel = ppsNumberService.addNewRecords(ppsNumberModel);
		assertThat(messageModel).isNotNull();
		assertThat(messageModel.getCode()).isEqualTo(0);
	}
	
	@Test(expected = TransactionSystemException.class)
	public void addNewPpsnumberCaseNameLengthKO() {
		
		PpsNumberModel ppsNumberModel = new PpsNumberModel();
		ppsNumberModel.setName("Abcdefghilmnopqrstuvzqwertyuioplkjhg");
		ppsNumberModel.setMobilePhoneNumber("0834234322");
		ppsNumberModel.setPpsNumber("1231231ADEFRFT");
		ppsNumberModel.setDateOfBirth(new Date());
		 ppsNumberService.addNewRecords(ppsNumberModel);
	} 

	@Test(expected = TransactionSystemException.class)
	public void addNewPpsnumberCasePhoneNumberLengthKO() {
		
		PpsNumberModel ppsNumberModel = new PpsNumberModel();
		ppsNumberModel.setName("Salvo");
		ppsNumberModel.setMobilePhoneNumber("0834234322");
		ppsNumberModel.setPpsNumber("1231231ADEFRFT");
		ppsNumberModel.setDateOfBirth(new Date());
		 ppsNumberService.addNewRecords(ppsNumberModel);
	 
	} 

	@Test(expected = TransactionSystemException.class)
	public void addNewPpsnumberCasePhoneNumberStartWith08KO() {
		
		PpsNumberModel ppsNumberModel = new PpsNumberModel();
		ppsNumberModel.setName("Salvo");
		ppsNumberModel.setMobilePhoneNumber("0734234322");
		ppsNumberModel.setPpsNumber("1231231ADEFRFT");
		ppsNumberModel.setDateOfBirth(new Date());
		 ppsNumberService.addNewRecords(ppsNumberModel);
	 
	}
	
	@Test(expected = TransactionSystemException.class)
	public void addNewPpsnumberCaseNameKO() {
		PpsNumberModel ppsNumberModel = new PpsNumberModel();
		ppsNumberModel.setName("");
		ppsNumberModel.setMobilePhoneNumber("0834234322");
		ppsNumberModel.setPpsNumber("1231231ADEFRFT");
		ppsNumberModel.setDateOfBirth(new Date());
		ppsNumberService.addNewRecords(ppsNumberModel);
	} 
	@Test(expected = TransactionSystemException.class)
	public void addNewPpsnumberCaseDuplicatePpsNumberKO() {
		PpsNumberModel ppsNumberModel = new PpsNumberModel();
		ppsNumberModel.setName("Salvo");
		ppsNumberModel.setMobilePhoneNumber("083423432211");
		ppsNumberModel.setPpsNumber("1231231ADEFRFT");
		ppsNumberModel.setDateOfBirth(new Date());
		PpsNumberModel ppsNumberModel2 = new PpsNumberModel();
		ppsNumberModel.setName("Luca");
		ppsNumberModel.setMobilePhoneNumber("083423432210");
		ppsNumberModel.setPpsNumber("1231231ADEFRFT");
		ppsNumberModel.setDateOfBirth(new Date());
		ppsNumberService.addNewRecords(ppsNumberModel2);
	}
	
	@Test
	@DirtiesContext
	public void viewListPpsNumber() {		 
		PpsNumberModel ppsNumberModel = new PpsNumberModel();
		ppsNumberModel.setName("Antonio");
		ppsNumberModel.setMobilePhoneNumber("081234567890");
		ppsNumberModel.setPpsNumber("1234567A");
		ppsNumberModel.setDateOfBirth(new Date());
		ppsNumberService.addNewRecords(ppsNumberModel);
		List<PpsNumberModel> ppsNumberModelList=ppsNumberService.findAll();
		assertThat(ppsNumberModelList.size()).isGreaterThan(0);
		assertThat(ppsNumberModelList.get(0).getName()).isEqualTo("Antonio");
		 
	}
}
