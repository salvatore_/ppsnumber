package com.quest.ppsnumber.service;

import java.util.List;

import com.quest.ppsnumber.model.MessageModel;
import com.quest.ppsnumber.model.PpsNumberModel;

public interface PpsNumberService {

	public MessageModel addNewRecords(PpsNumberModel ppsNumberModel);
	public List<PpsNumberModel> findAll();
}
