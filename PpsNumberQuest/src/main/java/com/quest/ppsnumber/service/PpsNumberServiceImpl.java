package com.quest.ppsnumber.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quest.ppsnumber.dao.repository.PpsNumberRepository;
import com.quest.ppsnumber.mapstruct.PpsNumberMapper;
import com.quest.ppsnumber.model.MessageModel;
import com.quest.ppsnumber.model.PpsNumberModel;

import org.springframework.transaction.annotation.Transactional;

@Service
public class PpsNumberServiceImpl implements PpsNumberService {
	@Autowired
	private PpsNumberRepository ppsNumberRepository;

	@Transactional()
	public MessageModel addNewRecords(PpsNumberModel ppsNumberModel) {
		MessageModel m = new MessageModel();
	 
		if(ppsNumberRepository.findByPpsNumber(ppsNumberModel.getPpsNumber())==null) {
			if(ppsNumberRepository.save(PpsNumberMapper.INSTANCE.pPpsNumberModelToPpsNumberEntity(ppsNumberModel)) !=null) {
				m.setCode(0);
				m.setMessage("Record inserted successfull");
			}
		}
		else {
			m.setCode(-1);
			m.setMessage("Pps number is already present");
		}		 
		return m;
	}
	@Transactional(readOnly = true) 
	public List<PpsNumberModel> findAll() {
		return PpsNumberMapper.INSTANCE
				.PpsNumberEntityTopPpsNumberModel(ppsNumberRepository.findAllOrderByCreationDate());
	}
}
