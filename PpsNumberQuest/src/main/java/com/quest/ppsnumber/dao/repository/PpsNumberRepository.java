package com.quest.ppsnumber.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.quest.ppsnumber.dao.entity.PpsNumberEntity;


@Repository
public interface PpsNumberRepository extends CrudRepository<PpsNumberEntity, Long> {
	public PpsNumberEntity findByPpsNumber(String ppsNumber);
	@Query("select pps from PpsNumberEntity pps order by pps.creationDate")
	public List<PpsNumberEntity> findAllOrderByCreationDate();
}
