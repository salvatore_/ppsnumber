package com.quest.ppsnumber.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "PPS_NUMBER_ENTITY")
@Getter @Setter
public class PpsNumberEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NotEmpty
	@Size(min = 1, max = 25)
	@Column(nullable = false,length=25)
	public String name;
	@Column(unique = true,nullable = false, length = 8)
	public String ppsNumber;
	@Column(nullable = false)
	@Past
	public Date dateOfBirth;
	@Column(nullable = false)
	@Pattern(regexp="08\\d{10}",message="{invalid.phonenumber}")
	public String mobilePhoneNumber;
	@Column(columnDefinition = "DATE DEFAULT CURRENT_DATE")
	public Date creationDate;
	 
}
