package com.quest.ppsnumber.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilsDate {

	
	public static Date getDataFromString(String dataIn) throws ParseException {
		if(dataIn==null) return new Date();
	    return new SimpleDateFormat("dd/MM/yyyy").parse(dataIn);  
	}
	
	
}
