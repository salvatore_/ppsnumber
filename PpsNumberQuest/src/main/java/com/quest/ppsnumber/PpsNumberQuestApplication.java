package com.quest.ppsnumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PpsNumberQuestApplication {

	public static void main(String[] args) {
		SpringApplication.run(PpsNumberQuestApplication.class, args);
	}

}
