package com.quest.ppsnumber.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quest.ppsnumber.model.MessageModel;
import com.quest.ppsnumber.model.PpsNumberModel;
import com.quest.ppsnumber.service.PpsNumberService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class PpsNumberRestController {
	@Autowired
	private PpsNumberService ppsNumberService;
	
	 @PostMapping("/api/addnewrecord")
	    public ResponseEntity<?> addNewRecord(
			 @RequestBody PpsNumberModel ppsNumberModel) {

	        MessageModel result = new MessageModel();
	        	result= ppsNumberService.addNewRecords(ppsNumberModel);	        
	        return ResponseEntity.ok(result);

	    }

}
