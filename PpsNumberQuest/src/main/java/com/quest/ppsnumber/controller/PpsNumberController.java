package com.quest.ppsnumber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.quest.ppsnumber.model.PpsNumberModel;
import com.quest.ppsnumber.service.PpsNumberService;

@Controller
public class PpsNumberController {
	//commit 1

	//commit 2 f
	//commit 3 prova test reset branch to here

	//prova

	@Autowired
	private PpsNumberService ppsNumberService;
	@GetMapping("/")
    public String getHome() {
        return "index";
    }
	
		@GetMapping({"/loadPageInsertPpsNumber"})
		public ModelAndView loadPageInsertPpsNumber() {	 
			ModelAndView mav = new ModelAndView("add_new_record");
			PpsNumberModel ppsNumberModel = new PpsNumberModel();
			    mav.addObject("ppsNumberModel", ppsNumberModel);		    
			    return mav;
		}  
	
	@GetMapping("/view_records")
    public ModelAndView getView_Records() {
		ModelAndView mav = new ModelAndView("/view_records");
		    mav.addObject("ppsNumberModelList", ppsNumberService.findAll());		    
		    return mav;
    }
}
