package com.quest.ppsnumber.model;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;


public class PpsNumberModel {

	@NotEmpty
	@Size(min = 1, max = 25)
	private String name;
	@NotEmpty
	@Size(min = 8, max = 8)
	private String ppsNumber;
	@Past
	@JsonFormat
    (shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date dateOfBirth;
	@Pattern(regexp="08\\d{10}",
	             message="Format number doesn't correct")
	private String mobilePhoneNumber;
	
	public PpsNumberModel() {};
	
	public PpsNumberModel(@NotEmpty @Size(min = 1, max = 25) String name,
			@NotEmpty @Size(min = 8, max = 8) String ppsNumber, @Past Date dateOfBirth,
			@Pattern(regexp = "08\\d{10}", message = "Format number doesn't correct") String mobilePhoneNumber) {
		super();
		this.name = name;
		this.ppsNumber = ppsNumber;
		this.dateOfBirth = dateOfBirth;
		this.mobilePhoneNumber = mobilePhoneNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPpsNumber() {
		return ppsNumber;
	}
	public void setPpsNumber(String ppsNumber) {
		this.ppsNumber = ppsNumber;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}
	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}
	
}
