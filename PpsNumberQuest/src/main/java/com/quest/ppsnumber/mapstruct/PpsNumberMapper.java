package com.quest.ppsnumber.mapstruct;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.quest.ppsnumber.dao.entity.PpsNumberEntity;
import com.quest.ppsnumber.model.PpsNumberModel;

@Mapper 
public interface PpsNumberMapper {
	
	PpsNumberMapper INSTANCE = Mappers.getMapper( PpsNumberMapper.class );
	PpsNumberEntity pPpsNumberModelToPpsNumberEntity(PpsNumberModel pPpsNumberModel);
	
	List<PpsNumberModel> PpsNumberEntityTopPpsNumberModel(List<PpsNumberEntity> ppsNumberEntity);

}
